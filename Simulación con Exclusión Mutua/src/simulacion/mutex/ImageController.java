package simulacion.mutex;

import java.util.Random;

import simulacion.op.Chunk;
import simulacion.op.ImageOperator;

public class ImageController extends Thread{
	
	ImageOperator auxImgOp;
	Lock lock;
	Random r;
	int idThread;
	Chunk auxChunk;
	boolean bloqueAsignado;
	
	public ImageController(ImageOperator auxImgOp, Lock lock, int idThread) {
		r = new Random();
		this.lock = lock;
		this.auxImgOp = auxImgOp;
		this.idThread = idThread;
		this.bloqueAsignado = false;
	}
	
	private void nonCriticalRegion() {
		auxImgOp.processChunk();
		Util.mySleep( r.nextInt( 1000 ) );
	}
	
	private void criticalRegion() 
	{
		if(auxImgOp.getChunkCounter() > 0)
		{
			if(!bloqueAsignado)
			{
				if(auxImgOp.searchChunk())
					bloqueAsignado = true;
			}
			else
			{
				// Almacenar en un archivo la imagen
				auxImgOp.almacenarImg();
				// Mostrar imagen actualizada 
				auxImgOp.mostrarImagen();
				// Decrementa chunkConter
				auxImgOp.decrementarCounter();
				// Cambiar el estatus del proceso
				bloqueAsignado = false;
			}
		}
		Util.mySleep( r.nextInt( 1000 ) );
		/*
		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		auxImgOp.searchChunk();*/
	}
	
	public void run() 
	{
		while (auxImgOp.getChunkCounter() > 0) 
		{
			lock.requestCR(idThread);
			criticalRegion();
			
			// System.out.println(idThread+" "+auxImgOp.getChunkCounter());
					
			lock.releaseCR(idThread);
			nonCriticalRegion();
		}
	}

}
